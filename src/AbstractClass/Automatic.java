package AbstractClass;

public abstract class Automatic {
    abstract void tataCost();

    abstract void hyundaiCost();

    abstract void toyatoCost();

    abstract void tataCost1();

    abstract void hyundaiCost1();

    abstract void toyatoCost1();
}
