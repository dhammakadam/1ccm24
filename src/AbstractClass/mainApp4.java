package AbstractClass;

import java.util.Scanner;

public class mainApp4 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("select service provider");
        System.out.println("1: AirIndia\t\t2:indigo");
        int choice=sc1.nextInt();
        if (choice==1 || choice==2){
            System.out.println("select route");
            System.out.println("0: pune-banglore");
            System.out.println("1:mumbai-chennai");
            System.out.println("2:kolkata-delhi");

            int routechoice=sc1.nextInt();
            System.out.println("enter total number of ticket");
            int count=sc1.nextInt();
            flightService service=null;
            if(choice==1) {
                service = new AirIndia();
            }
            else if (choice==2){
                service=new Indigo();
            }
            service.bookTicket(count,routechoice);
        }
        else {
            System.out.println("invalid service provider");
        }
    }
}
