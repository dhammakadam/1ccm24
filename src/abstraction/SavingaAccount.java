package abstraction;

public class SavingaAccount implements Account {
    double accountBalance=0.0;
    //account creation
public SavingaAccount(double accountBalance) {
    this.accountBalance = accountBalance;
    System.out.println("saving account created");
}

    @Override
    public void deposite(double amt) {
    accountBalance+=amt;
        System.out.println(amt+"rs created to your account");

    }

    @Override
    public void withdraw(double amt) {
    if (amt<=accountBalance){
        accountBalance-=amt;
        System.out.println(amt+"rs debited from your account");
    }else {
        System.out.println("insufficient balance");
    }
    }

    @Override
    public void ckeckBalance() {
        System.out.println("account balance is "+accountBalance+"rs");

    }
}
