package abstraction;

public class LoanAccount implements Account{
    double LoanAmount=0.0;
    //account created
    public LoanAccount(double LoanAmount){
        this.LoanAmount=LoanAmount;
        System.out.println("loan amount created");
    }
    @Override
    public void deposite(double amt) {
        LoanAmount-=amt;
        System.out.println(amt+"rs debited from LoanAccount");

    }

    @Override
    public void withdraw(double amt) {
        LoanAmount+=amt;
        System.out.println(amt+"rs credited to your account");
    }
    @Override
    public void ckeckBalance() {
        System.out.println("Active Loan Amount "+LoanAmount+"rs");
    }
}
