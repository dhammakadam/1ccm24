package abstraction;

 import jdk.nashorn.internal.runtime.regexp.JoniRegExp;
import sun.security.smartcardio.SunPCSC;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Account Type");
        System.out.println("1:Saving\t\t2:Loan");
        int choice=sc1.nextInt();
        System.out.println("Enter Account Opening Balance");
        double balance=sc1.nextInt();
        //create object of account factory
        AccountFactory Factory =new AccountFactory();
        Account accRef= Factory.CreateAccount (choice,balance);
        System.out.println("===================================");
        //transaction
        boolean status=true;
        while (status){
            System.out.println("Select Mode of Transaction");
            System.out.println("1:Deposit");
            System.out.println("2:Withdraw");
            System.out.println("3:Check balance");
            System.out.println("4:Exit");
            int mode=sc1.nextInt();
            switch (mode){
                case 1:
                    System.out.println("Enter Amount");
                    double depositeAmt=sc1.nextDouble();
                    accRef.deposite(depositeAmt);
                    break;
                case 2:
                    System.out.println("Enter Amount");
                    double withdrawAmt=sc1.nextDouble();
                    accRef.withdraw(withdrawAmt);
                    break;
                case 3:
                    accRef.ckeckBalance();
                    break;
                case 4:
                    status=false;
            }
    }
    }
}
