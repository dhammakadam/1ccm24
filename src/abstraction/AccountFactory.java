package abstraction;

public class AccountFactory {
    Account CreateAccount(int choice,double amt){
        Account a1=null;
        if(choice==1){
            a1=new SavingaAccount(amt);//upcasting
        }else if(choice==2){
            a1=new LoanAccount(amt);//upcasting
        }
        //return the object reference to main class
        return a1;
    }
}
